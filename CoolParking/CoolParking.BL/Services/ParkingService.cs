﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
	public class ParkingService : IParkingService
	{
		public ParkingService()
		{
			transactions = new List<TransactionInfo>();
			logService = new LogService();
			parking = new Parking();
		}
		private static LogService logService;
		public Parking parking;
		private List<TransactionInfo> transactions;
		public Vehicle GetVehicle(string id) => parking.Vehicles.Find(c => c.Id == id);

		public void AddVehicle(Vehicle vehicle)
		{
			if (vehicle == null)
			{
				throw new ArgumentNullException(nameof(vehicle));
			}
			if (GetFreePlaces() == 0)
			{
				throw new InvalidOperationException("Немає вільних місць.");
			}
			else
			{

				//Parking.Instance.Vehicles.Add(vehicle);
				parking.Vehicles.Add(vehicle);
				//parking.Vehicles.Add(vehicle);
			}

		}

		public decimal GetBalance()
		{
			return parking.Balance;
		}

		public int GetCapacity()
		{
			return Settings.ParkingSpace;
		}

		public int GetFreePlaces()
		{
			return Settings.ParkingSpace - parking.Vehicles.Count();
			//return Settings.ParkingSpace - parking.Vehicles.Count();
		}

		public TransactionInfo[] GetLastParkingTransactions()
		{
			DateTime now = DateTime.Now;
			foreach (var item in Enumerable.Reverse(transactions))
			{
				if (now - item.TimeOfTransaction > Settings.PeriodOfLogging)
				{
					break;
				}
				return null;
			}
			return transactions.ToArray();
		}

		public ReadOnlyCollection<Vehicle> GetVehicles()
		{
			return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
		}

		public string ReadFromLog()
		{
			return logService.Read();
		}

		public void RemoveVehicle(string vehicleId)
		{
			Vehicle vehicle = GetVehicle(vehicleId);
			//int index = vehicles.IndexOf(vehicle);
			int index = parking.Vehicles.IndexOf(vehicle);
			if (vehicle == null)
			{
				throw new ArgumentNullException(nameof(vehicle));
			}
			if (index >= 0)
			{
				if (vehicle.Balance < 0)
				{
					throw new ArgumentOutOfRangeException("Баланс менше нуля.");
				}
				parking.Vehicles.RemoveAt(index);
			}
		}

		public void TopUpVehicle(string vehicleId, decimal sum)
		{
			Vehicle vehicle = GetVehicle(vehicleId);
			if (sum <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(sum), "Сума поповнення повинна бути більша нуля.");
			}
			if (vehicle != null)
			{
				vehicle.Balance += sum;
			}
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}
				disposedValue = true;
			}
		}
		public void Dispose()
		{
			Dispose(true);
		}
		#endregion
	}
}