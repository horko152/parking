﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
	public class TimerService : ITimerService
	{
        public double Interval { get => this.Interval; set => value = Settings.PeriodOfPaying.TotalSeconds; }

        private readonly List<Vehicle> vehicles;
        private Parking parking;
        private LogService logService;
        private readonly Dictionary<VehicleType, decimal> types = new Dictionary<VehicleType, decimal>();

        public event ElapsedEventHandler Elapsed;

        private void TimerParkingPayment_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (Vehicle vehicle in vehicles)
            {
                var parkingPrice = types.FirstOrDefault(x => x.Key == vehicle.VehicleType);
                if (parkingPrice.Key == vehicle.VehicleType)
                {
                    if (parkingPrice.Value <= vehicle.Balance)
                    {
                        vehicle.Balance -= parkingPrice.Value;
                        parking.Balance += parkingPrice.Value;
                        string output = DateTime.Now + vehicle.Id + parkingPrice.Value;
                        logService.Write(output);
                    }
                    else
                    {
                        vehicle.Balance -= vehicle.Balance + (parkingPrice.Value - vehicle.Balance) * Convert.ToDecimal(Settings.PenaltyRatio);
                    }
                }
            }
        }


        public void Start()
        {
            Elapsed += TimerParkingPayment_Elapsed;
        }

        public void Stop()
        {
            Elapsed = null;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}