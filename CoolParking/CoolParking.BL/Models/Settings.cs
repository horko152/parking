﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
	public static class Settings
	{
		static Settings()
		{

		}

		private static decimal firstBalance = 0;
		private static int parkingSpace = 10;
		private static TimeSpan periodOfPaying = TimeSpan.FromSeconds(5);
		private static TimeSpan periodOfLogging = TimeSpan.FromMinutes(1);
		private static double penaltyRatio = 2.5;

		public static readonly Dictionary<VehicleType, decimal> prices = new Dictionary<VehicleType, decimal>()
		{
			{ VehicleType.PassengerCar, 2 },
			{ VehicleType.Truck, 5 },
			{ VehicleType.Bus, 3.5m },
			{ VehicleType.Motorcycle, 1 }
		};

		public static TimeSpan PeriodOfPaying => periodOfPaying;
		public static int ParkingSpace => parkingSpace;
		public static double PenaltyRatio => penaltyRatio;
		public static TimeSpan PeriodOfLogging => periodOfLogging;
		public static decimal FirstBalance => firstBalance;

	}
}