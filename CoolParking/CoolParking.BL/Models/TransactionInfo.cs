﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
	public struct TransactionInfo
	{
		public TransactionInfo(DateTime timeOfTransaction, string carId, decimal sum)
		{
			TimeOfTransaction = timeOfTransaction;
			CarId = carId;
			Sum = sum;
		}
		public decimal Sum { get; private set; }

		public DateTime TimeOfTransaction { get; private set; }
		public string CarId { get; private set; }

	}
}