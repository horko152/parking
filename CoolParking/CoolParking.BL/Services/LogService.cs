﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System.IO;
using System.Text;

namespace CoolParking.BL.Services
{
	public class LogService : ILogService
	{
		private string logFilePath;

		public LogService()
		{
		}

		public LogService(string logFilePath)
		{
			this.logFilePath = logFilePath;
		}

		public string LogPath => "./Transactions.log";

		public string Read()
		{
			return File.ReadAllText(LogPath, Encoding.UTF8);
		}

		public void Write(string logInfo)
		{
			using (StreamWriter writer = new StreamWriter(LogPath, true))
			{
				writer.Write(logInfo);
			}
		}
	}
}