﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    public class ParkingController : Controller
    {
        private readonly ParkingService parkingService;
        public ParkingController(ParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        [HttpGet]
        [Route("~/api/parking/balance")]
        public decimal GetBalance()
        {
            var balance = parkingService.GetBalance();
            return balance;
        }
        [HttpGet]
        [Route("~/api/parking/capacity")]
        public int GetCapacity()
        {
            var capacity = parkingService.GetCapacity();
            return capacity;
        }
        [HttpGet]
        [Route("~/api/parking/freePlaces")]
        public int GetFreePlaces()
        {
            var freeplaces = parkingService.GetFreePlaces();
            return freeplaces;
        }
        [HttpGet]
        [Route("~/api/vehicles")]
        public ActionResult<List<Vehicle>> GetVehicles()
        {
            return parkingService.GetVehicles().ToList();
        }
        [HttpGet]
        [Route("~/api/vehicles/{id}")]
        public ActionResult<Vehicle> GetVehiclesById([FromRoute] string id)
        {
            if (id == parkingService.GetVehicle(id).Id)
            {
                var vehicles = parkingService.GetVehicle(id);
                if (vehicles == null)
                {
                    return NotFound("Vehicle does not exist");
                }
                else
                {
                    return vehicles;
                }
            }
            else
            {
                return BadRequest("Wrong Id");
            }
        }
        [HttpPost]
        [Route("~/api/vehicles")]
        public ActionResult<Vehicle> AddVehicle(Vehicle vehicle)
        {
            parkingService.AddVehicle(vehicle);
            return vehicle;
        }
        [HttpDelete]
        [Route("~/api/vehicles")]
        public ActionResult<Vehicle> DeleteVehicle(string id)
        {
            if (id == parkingService.GetVehicle(id).Id)
            {
                var vehicles = parkingService.GetVehicle(id);
                if (vehicles == null)
                {
                    return NotFound("Vehicle does not exist");
                }
                else
                {
                    parkingService.RemoveVehicle(id);
                    return NoContent();
                }
            }
            else
            {
                return BadRequest("Wrong Id");
            }
        }

        [HttpGet]
        [Route("~api/transactions/last")]
        public ActionResult<TransactionInfo[]> GetLastTransactions()
        {
            return parkingService.GetLastParkingTransactions();
        }

        [HttpGet]
        [Route("~api/transactions/all")]
        public ActionResult<string> GetAllTransactions()
        {
                return parkingService.ReadFromLog();
        }
        [HttpPut("{id}")]
        [Route("~api/transactions/topUpVehicle/{id}")]
        public ActionResult<Vehicle> PutTopUpVehicle(string id, decimal sum)
        {
            if (id == parkingService.GetVehicle(id).Id)
            {
                parkingService.TopUpVehicle(id, sum);
                return parkingService.GetVehicle(id);
            }
            else
            {
                return NotFound();
            }
        }

    }
}