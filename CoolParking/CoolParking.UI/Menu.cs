﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace CoolParking.UI
{
	class Menu
	{
        public Menu()
        {
            parkingService = new ParkingService();
            //vehicles = new List<Vehicle>();
        }
        public static HttpClient webClient = new HttpClient();
        private static ParkingService parkingService;
        //private static List<Vehicle> vehicles;
        private static Random rnd = new Random();
        public static void Show()
        {
            Console.Title = "CoolParking";
            bool reshowMainMenu = true;

            while (true)
            {
                if (reshowMainMenu)
                    ShowMainMenu();

                reshowMainMenu = true;
                var cki = Console.ReadKey(true);

                switch (Char.ToUpper(cki.KeyChar))
                {
                    case '0':
                        ShowCarsMenu();
                        break;
                    case '1':
                        ShowAddCarMenu();
                        break;
                    case '2':
                        ShowRemoveCarMenu();
                        break;
                    case '3':
                        ShowRechargeCarBalanceMenu();
                        break;
                    case '4':
                        ShowTransactionHistoryForLastMinuteMenu();
                        break;
                    case '5':
                        ShowParkingBalanceMenu();
                        break;
                    case '6':
                        ShowParkingDebitForLastMinute();
                        break;
                    case '7':
                        ShowCountFreeParkingSpaceMenu();
                        break;
                    case '8':
                        ShowTransactionsLogMenu();
                        break;
                    case 'Q':
                        if (ShowAskExitMenu())
                            return;
                        break;
                    default:
                        reshowMainMenu = false;
                        break;
                }
            }
        }
        private static void ShowMainMenu()
        {
            ClearConsole();
            ShowTitle("Menu:");
            Console.WriteLine("0 - Вивести список машин на парковці");
            Console.WriteLine("1 - Поставити транспортний забіс");
            Console.WriteLine("2 - Забрати транспортний засіб");
            Console.WriteLine("3 - Поповнити баланс транспорту");
            Console.WriteLine("4 - Вивести трансакції за поточний період");
            Console.WriteLine("5 - Вивести поточний баланс паркінгу");
            Console.WriteLine("6 - Вивести суму зароблених грошей");
            Console.WriteLine("7 - Вивести кількість вільних міст");
            Console.WriteLine("8 - Вывести Transactions.log");
            ShowError("Q - Выйти");
        }
        private static void ShowCarsMenu()
        {
            HttpResponseMessage httpResponce = webClient.GetAsync("http://localhost:44354/api/vehicles").Result;
            var vehicles = httpResponce.Content.ReadAsAsync<List<Vehicle>>().Result;
            ClearConsole();
            ShowTitle("Список машин:");
            Console.WriteLine(ShowVehicles());
            if (vehicles.Count == 0)
            {
                ShowError("Машин немає");
                WaitForPressAnyKey();
            }
            else
            {
                Console.WriteLine(ShowVehicles());
                WaitForPressKeyEnter();
            }
        }
        private static string ShowVehicles()
        {
            HttpResponseMessage httpResponce = webClient.GetAsync("http://localhost:44354/api/vehicles").Result;
            var vehicles = httpResponce.Content.ReadAsAsync<List<Vehicle>>().Result;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"|{"ID",-10}|{"Баланс",-10}|{"Тип",-16}|");
            foreach (Vehicle vehicle in vehicles)
            {
                sb.AppendLine($"|{vehicle.Id,-10}|{Decimal.Round(vehicle.Balance, 10),-10}|{vehicle.VehicleType,-16}|");
            }

            return sb.ToString();
        }
        private static void ShowAddCarMenu()
        {
            ClearConsole();
            ShowTitle("Додати транспортний засіб на парковку");
            HttpResponseMessage httpResponce = webClient.GetAsync("http://localhost:44354/api/vehicles").Result;
            var vehicles = httpResponce.Content.ReadAsAsync<List<Vehicle>>().Result;
            //if (parkingService.GetFreePlaces() == 0)
            //{
            //    ShowError("Немає вільних місць");
            //    WaitForPressAnyKey();
            //    return;
            //}

            VehicleType vehicleType = (VehicleType)rnd.Next(1, 5);
            decimal initBalance = rnd.Next(50, 1001);

            Vehicle newVehicle = new Vehicle(vehicleType, initBalance);
            HttpResponseMessage httpPostResponce = webClient.PostAsJsonAsync("http://localhost:44354/api/vehicles", newVehicle).Result;
            //parkingService.AddVehicle(newVehicle);

            ShowInfo("Машина успішно додана:\r\n" + ShowVehicles());
            WaitForPressAnyKey();
        }

        private static void ShowRemoveCarMenu()
        {
            HttpResponseMessage httpResponce = webClient.GetAsync("http://localhost:44354/api/vehicles").Result;
            var vehicles = httpResponce.Content.ReadAsAsync<List<Vehicle>>().Result;
            ClearConsole();
            ShowTitle("Видалення машини з парковки");
            if (vehicles.Count == 0)
            {
                ShowError("На парковці відсутні машини");
                WaitForPressAnyKey();
                return;
            }

            do
            {
                Console.Write("Введите ID машины: ");
                var idVehicle = Console.ReadLine();
                HttpResponseMessage httpGetResponse = webClient.GetAsync("http://localhost:44354/api/vehicles/" + idVehicle).Result;
                ShowInfo("Машина успішно видалена.");
                WaitForPressAnyKey();
                return;
            } while (Console.ReadKey(true).Key != ConsoleKey.Q);
        }

        private static void ShowRechargeCarBalanceMenu()
        {
            ClearConsole();
            ShowTitle("Поповнити баланс машини");
            Console.Write("Введіть ID машини: ");
            var idVehicle = Console.ReadLine();
            Console.Write("Введіть суму: ");
            var sum = Console.ReadLine();
            HttpResponseMessage httpResponce = webClient.PutAsJsonAsync("http://localhost:44354/api/transactions/topUpVehicle/" + idVehicle, idVehicle).Result;
        }

        private static void ShowTransactionHistoryForLastMinuteMenu()
        {
            ClearConsole();

            ShowTitle("Історія трансакцій:");
            HttpResponseMessage httpGetResponse = webClient.GetAsync("http://localhost:44354/api/transactions/last").Result;
        }

        private static void ShowParkingBalanceMenu()
        {
            ClearConsole();
            ShowTitle("Загальний дохід парковки: " + webClient.GetAsync("http://localhost:44354/api/parking/balance").Result);

            WaitForPressAnyKey();
        }

        private static void ShowParkingDebitForLastMinute()
        {
            ClearConsole();
            ShowTitle("Дохід за останню хвилину складає: " + webClient.GetAsync("http://localhost:44354/api/parking/balance").Result);

            WaitForPressAnyKey();
        }

        private static void ShowCountFreeParkingSpaceMenu()
        {
            ClearConsole();
            ShowTitle($"Кількість вільних місць на парковці: {webClient.GetAsync("http://localhost:44354/api/parking/freePlaces").Result} з {webClient.GetAsync("http://localhost:44354/api/parking/capacity").Result}.");

            WaitForPressAnyKey();
        }

        private static void ShowTransactionsLogMenu()
        {
            ClearConsole();

            ShowTitle("Файл трансакцій:" + webClient.GetAsync("http://localhost:44354/api/").Result);
            
        }

        private static bool ShowAskExitMenu()
        {
            ClearConsole();
            Console.WriteLine("Підтвердити вихід: y/n");

            return Console.ReadKey().Key == ConsoleKey.Y;
        }



        private static void ClearConsole()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
        }

        private static void WaitForPressAnyKey()
        {
            Console.WriteLine();
            Console.WriteLine("Для повернення натисніть будь-яку клавішу...");
            Console.ReadKey(true);
        }

        private static void WaitForPressKeyEnter()
        {
            Console.WriteLine();
            Console.WriteLine("Для повернення натисніть клаішу Enter...");

            do { } while (Console.ReadKey(true).Key != ConsoleKey.Enter);
        }

        private static void ShowError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private static void ShowInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private static void ShowTitle(string message)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
