﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text;

namespace CoolParking.BL.Models
{
	public class Vehicle
	{
		public Vehicle(string id, VehicleType vehicleType, decimal balance)
		{
			Id = id;
			VehicleType = vehicleType;
			Balance = balance;
		}
		public Vehicle(VehicleType vehicleType, decimal balance)
		{
			Id = GenerateRandomRegistrationPlateNumber();
			VehicleType = vehicleType;
			Balance = balance;
		}
		public string Id { get; private set; }
		public VehicleType VehicleType { get; private set; }
		public decimal Balance { get; internal set; }

		private static string GenerateRandomRegistrationPlateNumber()
		{
			const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			StringBuilder vehicleNumbers = new StringBuilder();
			Random random = new Random();
			for (int i = 0; i <= 1; i++)
			{
				var symbol = alphabet[random.Next(0, alphabet.Length)];
				vehicleNumbers.Append(symbol);
			}
			vehicleNumbers.Append("-");
			for (int i = 0; i <= 3; i++)
			{
				vehicleNumbers.Append(random.Next(0, 9));
			}
			vehicleNumbers.Append("-");
			for (int i = 0; i <= 1; i++)
			{
				var symbol = alphabet[random.Next(0, alphabet.Length)];
				vehicleNumbers.Append(symbol);
			}
			return vehicleNumbers.ToString();
		}
	}
}